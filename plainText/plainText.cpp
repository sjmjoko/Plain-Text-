#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include<bitset>
#include<vector>
#include<array>
#include<algorithm>
using namespace std;
template < class T >
void print(vector<T> v)
{
	for (int i = 0; i < v.size(); i++)
	{
		cout << v[i] << "";  //open spaces again
	}
	cout << endl;
}
template<class X>
void print2D(vector< vector<X>> x)
{
	for (int i = 0; i < x.size(); i++)
	{
		for (int j = 0; j < x[0].size(); j++)
		{
			cout << x[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}
void ip1_permutation(vector<vector<int>> &a, int c[]) //vector confused,vector non confused, ip table
{
	for (int i = 0; i< a.size();i++)
		for (int j = 0;j < a[0].size(); j++)
	{
		a[i][j] = a[i][c[j] - 1];
	}
}
void deleteSpaces(vector<char> & v)
{
	for (int k = 0; k < v.size(); k++)
	{
		if (v[k] == ' ')
		{
		v.erase(v.begin() + k);
		}
	}
}
void addFillers(vector<char> & v)
{
	while (v.size() % 8 != 0)
		{
			v.insert(v.end(), 'x');    
		}
}
void toAscii(vector<vector <char> > &c, vector<vector < int> > &v, int rownum)
{
	int ascii;
	for (int i = 0; i <rownum; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			ascii = int(c[i][j]);
			v[i][j] = ascii;
		}
	}
}
template<class Y>
void Resize(vector<vector<Y> > &v, int rows, int columns)
{
	v.resize(rows);
	for (int i = 0; i < rows; ++i)
	{
		v[i].resize(columns);   //allocate space for columns
	}
}
void expansion(vector<int> &right, vector<int> temp)
{
	int arr[] = { 32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17,
		16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1 };
	for (int i = 0; i < 48; i++)
	{
		right.push_back(temp[arr[i]-1]);
	}
}
void XOR(vector<vector<int> > &k, vector<int> &t, vector<int> &c,int rowkey) //pass the row of the key being used
{
	int a, b;
	for (int i = 0; i < k[rowkey].size();i++)
	{
		a = k[rowkey][i];
		b = t[i];
		c.push_back(a^b);
	}
}
void toBinary(vector<vector<int> > &ascii, vector<vector<int> > &bin,vector<int> &mes, int rows)
{
	int b = 0, c = 7, e = 0;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < ascii[0].size(); j++)
		{
			bitset<8> p(ascii[i][j]);
			for (int a = 0; a < ascii[0].size(); a++)
			{
				mes.push_back(p[c]);
				c--;
			}
			for (int d = 0; d < ascii[0].size(); d++)
			{
				bin[e][d + b] = mes[d];
			}
			b += 8;
			if (b == 64)
			{
				b -= 8;
			}
			c = 7;
			mes.clear();
		}
		b = 0;
		e++;
	}
}
void encryption(vector<vector<int> > &text, vector<vector<int> > &key, vector<int> cipher, int textRows)  //study this function
{
	int cycle = 0; //determines the number of rows to be encrypted
	while (cycle < textRows)
	{
		int keyIterator = 0;
		vector<int> Left, Right, temp,tempL; //temp is usd to store things temporarily
		//separate the text into 32 bits, left and right
		for (int i = 0; i < 32; i++)  //select the row of the text
		{
			Left.push_back(text[cycle][i]);
			Right.push_back(text[cycle][i + 32]);  //push the right text as a temporary text 32 bit
		}
		tempL.insert(tempL.end(), Right.begin(), Right.end()); //R(i)=L(i+1)
		//this part should be repeated 16 times
		expansion(temp, Right); //temp is the temporary expanded right text
		Right.clear();//clear the right variable to store the new Right text
		XOR(key, temp, Right, cycle);
	}
}
int main()
{
	vector<vector<int> > textBin,textAscii,textMsg; //characters each of block 8 
	vector<vector<char> > textChar;
	string message;    //string message
	vector<char> msg;  //vector message
	vector<int> asciiMsg, msgTemp,ipMsg;
	int IP_1[] = { 58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8,
		57, 49, 41, 33, 25, 17, 9, 1, 9, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7 };
	int IP_2[] = { 40,8,48,16,56,24,64,32,39,7,47,15,55,23,63,31,38,6,46,14,54,22,62,30,37,5,45,13,53,21,61,29,36,
		           4,44,12,52,20,60,28,35,3,43,11,51,19,59,27,34,2,42,10,50,18,58,26,33,1,41,9,49,17,57,25};
	int expansion[] = { 32,1,2,3,4,5,4,5,6,7,8,9,8,9,10,11,12,13,12,13,14,15,16,17,
		             16,17,18,19,20,21,20,21,22,23,24,25,24,25,26,27,28,29,28,29,30,31,32,1};
	int k = 7, z = 0,l=0;
	char t;
	cout << "Enter the message: ";
	getline(cin, message);

	//transfer string message to string message
	for (int i = 0; i < message.length(); i++)
	{
		msg.push_back(message[i]);
	}
	//delete spaces in the message
	deleteSpaces(msg);
	//add filler character if message is not in blocks of 8
	addFillers(msg);
	int rows = msg.size() / 8;
	//resize vectors
	Resize(textChar, rows, 8);
	Resize(textAscii, rows, 8);
	Resize(textBin, rows, 64);
	//transfer the message to a 2D character vector
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			t = msg[j + l];
			textChar[i][j] = t;
		}
		l += 8;
	}
	//convert the chars to ascii code for each block
	toAscii(textChar, textAscii, rows);
	//convert the ascii message into binary
	toBinary(textAscii,textBin,msgTemp,rows);
    //IP-1 permutation
	ip1_permutation(textBin, IP_1);
	cout << textBin[0].size() << endl;
	/*print2D(textBin);*/

	system("pause");
	return 0;
}